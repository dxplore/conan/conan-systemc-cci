from conans import ConanFile, tools, AutoToolsBuildEnvironment
from conans.errors import ConanInvalidConfiguration
import os


class SystemccciConan(ConanFile):
    name = "SystemC-CCI"
    version = "1.0.0"
    description = """The SystemC Verification library provides a common set of
                     APIs that are used as a basis to verification activities
                     with SystemC (generation of values under constraints,
                     transaction recording, etc.)."""
    homepage = "https://www.accellera.org/"
    url = "https://gitlab.com/dxplore/conan-systemc-cci"
    license = "Apache-2.0"
    author = "Jan Van Winkel <jan.van_winkel@dxplore.eu>"
    topics = ("simulation", "modeling", "esl", "cci")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True}
    requires = "SystemC/2.3.3@dxplore/stable"
    generators = "make"
    exports_sources = "makefile.patch", "cci_param_untyped_handle.patch", \
                      "cci_value_converter.patch", "broker.patch", \
                      "rapidjson.patch"

    def configure(self):
        if tools.valid_min_cppstd(self, "17"):
            raise ConanInvalidConfiguration(
                "C++ Standard %s not supported by SystemC" %
                self.settings.compiler.cppstd)

        if not self.settings.compiler.cppstd:
            if tools.valid_min_cppstd(self, "14"):
                self.settings.compiler.cppstd = 14
            elif tools.valid_min_cppstd(self, "11"):
                self.settings.compiler.cppstd = 11
            elif tools.valid_min_cppstd(self, "98"):
                self.settings.compiler.cppstd = 98

    def source(self):
        src_dir = os.path.join(self.source_folder, "cci-%s" % self.version)
        url = "https://www.accellera.org/images/downloads/" + \
              "standards/systemc/cci-%s.zip" % self.version
        tools.get(url=url)

        patch = os.path.join(self.source_folder, "makefile.patch")
        tools.patch(base_path=src_dir, patch_file=patch)

        patch = os.path.join(self.source_folder,
                             "cci_param_untyped_handle.patch")
        tools.patch(base_path=src_dir, patch_file=patch)

        patch = os.path.join(self.source_folder,
                             "cci_value_converter.patch")
        tools.patch(base_path=src_dir, patch_file=patch)

        patch = os.path.join(self.source_folder,
                             "broker.patch")
        tools.patch(base_path=src_dir, patch_file=patch)

        patch = os.path.join(self.source_folder,
                             "rapidjson.patch")
        tools.patch(base_path=src_dir, patch_file=patch)

    def build(self):
        cci_dir = os.path.join(self.source_folder, "cci-%s" % self.version)
        cci_src_dir = os.path.join(cci_dir, "src")
        conan_make_file = os.path.join(self.build_folder,
                                       "conanbuildinfo.mak")

        env_build = AutoToolsBuildEnvironment(self)
        env_build.fpic = True
        args = ['CONAN_MAKE_FILE=%s' % conan_make_file]
        with tools.chdir(cci_src_dir):
            env_build.make(args=args, target='clean')
            env_build.make(args=args)

    def package(self):
        cci_dir = os.path.join(self.source_folder, "cci-%s" % self.version)
        lib_dir = os.path.join(cci_dir, "lib")
        src_dir = os.path.join(cci_dir, "src")
        self.copy("*.h", dst="include", src=src_dir)
        self.copy("cci_configuration", dst="include", src=src_dir)
        if self.options.shared:
            self.copy("*.so", dst="lib", src=lib_dir)
        else:
            self.copy("*.a", dst="lib", src=lib_dir)

    def package_info(self):
        self.cpp_info.libs = ["cciapi"]
